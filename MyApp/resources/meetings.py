import json

from cerberus import Validator
from flask import request
from flask_restful import Resource

from MyApp import helpers as hp
from MyApp import models as md
from MyApp.utils import Response

v = Validator()

# Request body validator schema
SCHEDULE_MEETING_SCHEMA_POST = {
    'title': {'required': True, 'type': 'string'},
    'participants': {
        'type': 'list',
        'schema': {
            'type': 'dict',
            'schema': {
                'name': {'type': 'string'},
                'email': {'type': 'string'},
                'rsvp': {'type': 'string'}
            }
        }
    },
    'start_time': {'required': False, 'type': 'string'},
    'end_time': {'required': False, 'type': 'string'}
}


class ScheduleMeeting(Resource):
    """
    Endpoint: /meetings
    Schedule new meetings by using POST request
    Fetch scheduled meetings with respect to participants and time-frame using GET request
    """
    def get(self):
        """
        1). Fetch scheduled meetings with respect to time-frame using GET request
            :Query-String: start: datetime format - YYYY-MM-DD HH:MM:SS - Mandatory field
            :Query-String: end: datetime format - YYYY-MM-DD HH:MM:SS - Mandatory field
        2). Fetch scheduled meetings with respect to participants using GET request
            :Query-String: start: datetime format - YYYY-MM-DD HH:MM:SS - Mandatory field
            :Query-String: end: datetime format - YYYY-MM-DD HH:MM:SS - Mandatory field
            :Query-String: participant: email of the participant whose meetings need to be checked - Mandatory field
        :return: List of meeting objects
        """
        response = Response()
        if request.args.get('start') is not None and request.args.get('end') is not None:
            if request.args.get('participant') is not None:
                select = md.Schedules.objects(
                    participant_email_id=request.args.get('participant'),
                    start_time__gte=request.args.get('start'),
                    end_time__lte=request.args.get('end')
                ).skip(
                    int(request.args.get('offset', 0))
                ).limit(
                    int(request.args.get('limit', 5))
                ).all()
                if len(select) > 0:
                    meetings = md.Meetings.objects.filter(
                        id__in=[obj["meeting_id"] for obj in json.loads(select.to_json())]
                    ).all()
                    response.data = json.loads(meetings.to_json())
                else:
                    response.code = 204
            else:
                select = md.Meetings.objects(
                    start_time__gte=request.args.get('start'),
                    end_time__lte=request.args.get('end')
                ).skip(
                    int(request.args.get('offset', 0))
                ).limit(
                    int(request.args.get('limit', 5))
                ).all()
                response.data = json.loads(select.to_json())
                if not len(response.data):
                    response.code = 204
        else:
            response.code = 400
            response.error_message = "Please provide start and end datetime"
        return response.response_json()

    def post(self):
        """
            Create new meeting
            :param: Refer SCHEDULE_MEETING_SCHEMA_POST schema
            :return: Newly scheduled meeting object
        """
        body = request.get_json()
        response = Response()
        res = v.validate(body, SCHEDULE_MEETING_SCHEMA_POST)
        if not res:
            response.error_message = str(v.errors)
            response.code = 400
            return response.response_json()
        from datetime import datetime as dt
        start_time = dt.strptime(body.get('start_time'), "%Y-%m-%d %H:%M:%S")
        end_time = dt.strptime(body.get('end_time'), "%Y-%m-%d %H:%M:%S")
        if end_time > start_time:
            new_meeting = hp.ScheduleMeeting(body)
            response = new_meeting.schedule_meeting()
        else:
            response.code = 404
            response.error_message = "Invalid start and end time"
        return response.response_json()


class Meeting(Resource):

    def get(self, id):
        """
            Fetch scheduled meeting with respect to the meeting ID using GET request
            :param: ID: Required meeting Meeting ID
            :return: Required meeting object
        """
        response = Response()
        select = md.Meetings.objects(
            id=id
        ).first()
        if select is not None:
            response.data = json.loads(select.to_json())
        else:
            response.code = 204
        return response.response_json()
