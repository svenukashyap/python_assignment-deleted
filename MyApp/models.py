from datetime import datetime
from enum import Enum

from mongoengine import *
import mongoengine_goodjson as gj

from MyApp.config import Config

connect(Config.DB_NAME)


class RSVP(Enum):
    YES = 'yes'
    NO = 'no'
    MAYBE = 'maybe'
    NOT_ANSWERED = 'not answered'


class Meetings(gj.Document):
    id = StringField(primary_key=True, required=True)
    title = StringField(required=True)
    participants = ListField(required=True)
    start_time = DateTimeField(required=True)
    end_time = DateTimeField(required=True)
    creation_timestamp = DateTimeField(required=True, default=datetime.now())


class Participants(gj.Document):
    name = StringField(required=True)
    email = EmailField(primary_key=True, required=True)


class Schedules(gj.Document):
    id = StringField(primary_key=True, required=True)
    meeting_id = StringField(required=True)
    participant_email_id = EmailField(required=True)
    start_time = DateTimeField(required=True)
    end_time = DateTimeField(required=True)
    rsvp = StringField(choices=[item.value for item in RSVP], required=True)
    creation_timestamp = DateTimeField(required=True, default=datetime.now())
