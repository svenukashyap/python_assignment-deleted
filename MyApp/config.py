import os
from sys import platform


class Config(object):
    IN_PRODUCTION = os.getenv('IN_PRODUCTION', False)

    PROJECT_NAME = os.getenv('PROJECT_NAME', 'Meeting-Scheduler')

    if platform == "linux" or platform == "linux2":
        SERVER_HOST = os.getenv("LINUX_SERVER_HOST", '0.0.0.0')
    elif platform == "win32":
        SERVER_HOST = os.getenv("WIN_SERVER_HOST", '127.0.0.1')

    SERVER_PORT = int(os.getenv("SERVER_PORT", 8000))

    DEBUG = False if IN_PRODUCTION else True

    BASE_URL = os.getenv('BASE_URL', f'http://localhost:{SERVER_PORT}')

    API_PREFIX = os.getenv('API_PREFIX', '/v1')

    SECRET_KEY = os.getenv('SECRET_KEY', 'you-will-never-guess')

    DB_NAME = os.getenv('DB_NAME', 'Meetings')

    LOGGING_CONFIG = {
        'version': 1,
        'disable_existing_loggers': False,
        'formatters': {
            'standard': {
                'format': '%(name)s:%(lineno)s %(message)s'
            },
        },
        'handlers': {
            'local': {
                'class': 'logging.StreamHandler',
                'formatter': 'standard',
            },
        },
        'loggers': {
            'MyApp': {
                'level': 'INFO' if IN_PRODUCTION else 'DEBUG',
            }
        },
    }
