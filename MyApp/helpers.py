from datetime import datetime as dt
import json

import MyApp.models as md
from MyApp.utils import Response, random_hash


class Participant(object):
    """
    :param: user_details: Contains user attributes like name and email-id of the participant
    :type: list of user-objects or user-details dict
    """

    def __init__(self, user_details):
        self.user_details = user_details

    def _save_participants(self):
        """
        Save the participants details into participants collection
        bulk insert or single document insert
        :return: :Success: newly added participants email-id
        :return: :Failure: Failure error code and appropriate error-message
        """
        response = Response()
        try:
            if isinstance(self.user_details, list):
                response.data = []
                for user in self.user_details:
                    md.Participants(
                        id=random_hash,
                        name=user.get('name'),
                        email=user.get('email')
                    ).save()
                    response.data.append(user.get('email'))
            else:
                md.Participants(
                    id=random_hash,
                    name=self.user_details.get('name'),
                    email=self.user_details.get('email')
                ).save()
                response.data.append = self.user_details.get('name')
        except Exception as error:
            response.exception_handler(error=str(error))
        return response


class Meeting(Participant):
    def __init__(self, meeting_obj):
        self.meeting_obj = meeting_obj
        super().__init__(self.meeting_obj.get('participants'))

    @staticmethod
    def _check_meeting_overlaps(user_details, start_time, end_time):
        """
            Check whether meetings are overlapping, ie. 1 participant shouldn't have 2 or more meetings with
            RSVP - yes
            :return: :Success: response-code 200 if their is no overlapping of the meetings.
            :return: :Failure: response-code 409 if any overlapping of the meetings.
        """
        response = Response()
        try:
            for participant in user_details:
                schedules = md.Schedules.objects(
                    participant_email_id=participant.get('email'),
                    start_time__gte=start_time,
                    end_time__lte=end_time,
                    rsvp="yes"
                )
                if len(schedules):
                    response.code = 409
                    response.error_message = "Meeting overlapping"
                    raise Exception("Meeting overlapping")
        except Exception as error:
            if response.code == 200:
                response.code = 400
            response.exception_handler(error=str(error), code=response.code)
        else:
            response.code = 200
        return response

    def _save_meeting(self, meeting_obj):
        """
           Save the meetings details into Meetings collection
           if their is no meeting overlapping for any of the participant
           :return: :Success: Newly added meeting json
           :return: :Failure: Failure error code and appropriate error-message
       """
        response = Response()
        try:
            response = self._check_meeting_overlaps(meeting_obj.get('participants'),
                                                    start_time=meeting_obj.get('start_time'),
                                                    end_time=meeting_obj.get('end_time'))
            if response.code == 200:
                add_participants = self._save_participants()
                if add_participants.code == 200 and isinstance(add_participants.data, list) and len(
                        add_participants.data):
                    new_meeting = md.Meetings(
                        id=random_hash(),
                        title=meeting_obj.get('title'),
                        participants=add_participants.data,
                        start_time=meeting_obj.get('start_time'),
                        end_time=meeting_obj.get('end_time'),
                        creation_timestamp=meeting_obj.get('creation_timestamp') if meeting_obj.get(
                            'creation_timestamp') is not None else dt.utcnow()
                    ).save()
                    response.data = json.loads(new_meeting.to_json(indent=2))
                else:
                    if not str(response.error_message).strip():
                        response.error_message = "Failed to add the participants"
                    return response
            else:
                if response.code == 400:
                    response.error_message = "Something went wrong"
        except Exception as error:
            response.exception_handler(error=str(error))
        return response


class ScheduleMeeting(Meeting):
    def __init__(self, meeting_obj):
        self.meeting_obj = meeting_obj
        super().__init__(self.meeting_obj)

    def schedule_meeting(self):
        """
           Schedule a new meeting into Schedule collection
           by checking meeting overlapping for any of the participant and adding new participants details into the
           Participants collection
           :return: :Success: newly added meeting json
           :return: :Failure: Failure error code and appropriate error-message
       """
        response = Response()
        last_inserted = []
        try:
            response = self._save_meeting(self.meeting_obj)
            if response.code == 200 and str(response.data).strip():
                for participant in self.meeting_obj['participants']:
                    new_schedule = md.Schedules(
                        id=random_hash(),
                        meeting_id=response.data.get('id'),
                        participant_email_id=participant.get('email'),
                        start_time=self.meeting_obj.get('start_time'),
                        end_time=self.meeting_obj.get('end_time'),
                        rsvp=participant.get('rsvp'),
                        creation_timestamp=self.meeting_obj.get('creation_timestamp') if self.meeting_obj.get(
                            'creation_timestamp') is not None else dt.utcnow()
                    ).save()
                    last_inserted.append(new_schedule.pk)
                response.message = "Meeting scheduled successfully."
            else:
                if not str(response.error_message).strip():
                    response.error_message = "Failed to schedule a meeting."
        except Exception as error:
            for id in last_inserted:
                md.Schedules.objects(id=id).delete()
            md.Meetings.objects(id=response.data.get('id')).delete()
            response = response.exception_handler(error=str(error), code=400)
        return response
