import json
import logging
import os
import uuid

from flask import jsonify, make_response

logger = logging.getLogger('root')


class Response:
    def __init__(self):
        self.code = 200
        self.data = None
        self.error_message = ''

    def exception_handler(self, error, code=400, message='Something went wrong, Please try again after sometime.',
                          sys=None):
        if sys is not None:
            exc_type, exc_obj, exc_tb = sys
            filename = os.path.split(exc_tb.tb_frame.f_code.co_filename)[1]
            logger.error(f'Message:{message}, Error:{error}, File-Name:{filename}, Line-Number:{exc_tb.tb_lineno}')
        logger.exception(error)
        self.error_message = str(error)
        self.code = code
        self.__delattr__('data')
        return self

    def response_json(self, headers=None):
        res = make_response(jsonify(**json.loads(json.dumps(self, default=lambda o: o.__dict__,
                                                            sort_keys=True, indent=4))), self.code)
        res.headers.extend(headers or {})
        return res

    def __repr__(self):
        if self.code in range(200,300):
            return f'<Response(Code={self.code}, Data={self.data})'
        else:
            return f'<Response(Code={self.code}, Data={self.data})'


def random_hash():
    return str(uuid.uuid4().hex)
