import logging.config

from flask import Flask
from flask import jsonify
from flask_restful import Api
from flask_cors import CORS

from MyApp.config import Config
from MyApp.resources import meetings as ms


# create and config MyApp
app = Flask(__name__)
app.config.from_object(Config)
CORS(app)
logging.config.dictConfig(app.config.get('LOGGING_CONFIG'))

# set up plugins
api = Api(app)


@app.route('/')
def index():
    return jsonify({'project': Config.PROJECT_NAME})


API_PREFIX = Config.API_PREFIX

api.add_resource(ms.ScheduleMeeting, f'{API_PREFIX}/meetings')

api.add_resource(ms.Meeting, f'{API_PREFIX}/meetings/<string:id>')
