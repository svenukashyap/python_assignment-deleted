﻿# Meeting Scheduler Backend

This project will exposes API's that users can create and view the scheduled meetings

### To Setup :

- (development stage) Clone locally `git clone https://gitlab.com/svenukashyap/python_assignment.git`
- Setup virtualenv (venv): `python3 -m venv venv` (Make sure you are on python 3.6 or higher)
- Activate: `source venv/bin/activate` (Make sure you are on pip-19.1 or higher)
- Install dependencies: `pip install -r requirements.txt`
- Create the local environment file `touch .env` and replace with the required.

### To run :

Development Stage: `python run.py`

Production Stage: Run bash script `run.sh` in background. // To be modified as per required.


## Doc
// To be separated out to ./doc

#### Project Structure

    ├───run.py (Main file to run)
    ├───.env (containing local env variables)
    ├───MyApp (Our Main app)
    │   ├───resources (General APIs)
    │   │   ├──__init__.py
    │   │   └──meetings.py
    │   ├───__init__.py (Initialising app)
    │   ├─── config.py (Contains all configurations)
    │   ├─── utils.py (Utilitiy classes and functions)
    │   ├─── helpers.py (All database helper classes)
    │   └─── models.py (All database collection and model initialised functions)
    │───tests
        └───__init__.py



#### Example `.env` file

```
PROJECT_NAME='Meeting-Scheduler'
IN_PRODUCTION=False
SERVER_PORT=8000
BASE_URL='0.0.0.0'
API_PREFIX='/v1'
SECRET_KEY=<hashed_string>
DB_NAME='Meetings'
```

### API Endpoints
```console
* POST http://localhost:5000/v1/meetings
* GET http://localhost:5000/v1/meetings?start=<start_datetime>&end=<end_datetime>
* GET http://localhost:5000/v1/meetings?start=<start_datetime>&end=<end_datetime>&participant=<email_id>

* GET http://localhost:5000/v1/meetings/:id
```

### Assumptions

* All datetimes are UTC
* datetime format is YYYY-MM-DD HH:MM:SS

### Response Structure
```
    {
      "code": 200,
      "data": {
        // Application-specific data would go here.
      },
      "error_message": null,  // err message object
      "message": "Success!!"  // message to display
    }
```
