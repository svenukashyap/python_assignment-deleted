from MyApp import app
from MyApp.config import Config


if __name__ == '__main__':
    app.run(host=Config.SERVER_HOST, port=5000, debug=Config.DEBUG)